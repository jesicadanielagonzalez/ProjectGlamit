# ProjectGlamit

## Tools BackEnd

- Java 1.8
- Maven
- Spring Boot
- Tomcat 8.5
- BD: H2

## Instalación
- Git : clonar 
  git@gitlab.com:jesicadanielagonzalez/ProjectGlamit.git
  Main es el branch actual de desarrollo.
  
- Cuando el proyecto se inicializa se crea la base de datos en memoria
con productos y categorias preestablecidos.

 ## Uso
 Es necesario levantar el proyecto desde un IDE acorde para poder realizar
 las pruebas. 
 IDE recomendado : IntelliJ
 Se crearon 3 endpoints con seguridad en header, a los fines practicos 
 se hardcodeo el user (user = glamit) y la pass (pass = 123). 
 Los endpoints son:
 
 1- Obtencion de todos los productos con la posibilidad de filtrar por SKU
 y paginado.
 
 curl --location --request GET 'http://localhost:8080/glamit/allProducts?size=2&page=1&sku=YJF38KJJ5JJ' \
 --header 'user: glamit' \
 --header 'pass: 123'
 
 2- Obtencion de todas las categorias existentes.
 
 curl --location --request GET 'http://localhost:8080/glamit/allCategories' \
 --header 'user: glamit' \
 --header 'pass: 123'
 
 3- Creacion de un nuevo producto
 
 curl --location --request POST 'http://localhost:8080/glamit/newProduct' \
 --header 'user: glamit' \
 --header 'pass: 123' \
 --header 'Content-Type: application/json' \
 --data-raw '{
     "name": "JESICA",
     "codeCategory": "ABC111",
     "price": 3.0,
     "urlImage": "4DA7C646-D7B4-B061-9953-79E42AEE117A",
     "sku": "SVF14L777"
 }'
 
 ## Metodologia de desarrollo
 - Programacion orientada a objetos 
 - Programacion en capas
 - Patron MVC
 - Patron de inyeccion de dependencia
 - Patron DTO
 - Patron repositorio
