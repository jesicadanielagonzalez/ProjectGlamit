package projectglamit.demo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import projectglamit.demo.models.Category;
import projectglamit.demo.models.Product;
import projectglamit.demo.models.ProductDTO;
import projectglamit.demo.services.GlamitServices;

import java.util.List;

@RestController
@RequestMapping("/glamit")
public class GlamitController {

    @Autowired
    GlamitServices glamitServices;

    @GetMapping(value = "/allProducts")
    public List<Product> getAllProducts(@RequestParam(required = false) String sku,
                                        @RequestParam(required = false, defaultValue = "10") Long size,
                                        @RequestParam(required = false, defaultValue = "1") Long page) {
        return glamitServices.findAllProducts(sku, size, page);
    }

    @GetMapping(value = "/allCategories")
    public List<Category> getAllCategories() {
        return glamitServices.findAllCategories();
    }

    @PostMapping(value = "/newProduct")
    public String postNewProduct(@RequestBody ProductDTO productDTO) {
        return glamitServices.saveNewProduct(productDTO) != null
                ? "Se guardo exitosamente."
                : "El producto ya existe.";
    }
}
