package projectglamit.demo.repositories;

import org.springframework.stereotype.Repository;
import projectglamit.demo.models.Category;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer>{

    Category findByUniqueCode(String uniqueCode);
}
