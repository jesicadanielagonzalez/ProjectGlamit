package projectglamit.demo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import projectglamit.demo.models.Category;
import projectglamit.demo.models.Product;
import projectglamit.demo.models.ProductDTO;
import projectglamit.demo.repositories.CategoryRepository;
import projectglamit.demo.repositories.ProductRepository;
import java.util.List;

@Service
public class GlamitServices {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    CategoryRepository categoryRepository;


    public List<Product> findAllProducts(String sku, Long size, Long page) {
        Pageable pageable= PageRequest.of(page.intValue(), size.intValue());
        return sku != null
                ? productRepository.findAllBySku(sku)
                : productRepository.findAll(pageable).getContent();
    }

    public List<Category> findAllCategories() {
        return categoryRepository.findAll();
    }


    public Category findCategoryByUniqueCode(String uniqueCode){
        return categoryRepository.findByUniqueCode(uniqueCode);
    }

    public Product saveNewProduct(ProductDTO productDTO) {
        if (productRepository.findAllBySku(productDTO.getSku()).isEmpty()){
            Product product = new Product(productDTO.getName(),
                    findCategoryByUniqueCode(productDTO.getCodeCategory()),
                    productDTO.getPrice(),
                    productDTO.getUrlImage(),
                    productDTO.getSku());
            return productRepository.save(product);
        } else {
            return null;
        }
    }
}
