package projectglamit.demo.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "CATEGORIAS", schema = "CATEGORIAS_GLAMIT")
public class Category {
    @Basic
    @Column(name = "NOMBRE")
    private String name;
    @Id
    @Column(name = "CODIGO_UNICO")
    private String uniqueCode;
    @OneToMany(mappedBy = "category")
    @JsonIgnore
    List<Product> listProducts;

    public Category() {
    }

    public Category(String name, String uniqueCode, List<Product> listProducts) {
        this.name = name;
        this.uniqueCode = uniqueCode;
        this.listProducts = listProducts;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUniqueCode() {
        return uniqueCode;
    }

    public void setUniqueCode(String uniqueCode) {
        this.uniqueCode = uniqueCode;
    }

    public List<Product> getListProducts() {
        return listProducts;
    }

    public void setListProducts(List<Product> listProducts) {
        this.listProducts = listProducts;
    }
}
