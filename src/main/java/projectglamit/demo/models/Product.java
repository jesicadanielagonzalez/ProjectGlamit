package projectglamit.demo.models;
import javax.persistence.*;

@Entity
@Table(name = "PRODUCTOS", schema = "PRODUCTOS_GLAMIT")
public class Product {
    @Basic
    @Column(name = "NOMBRE")
    private String name;
    @ManyToOne
    @JoinColumn(name = "CODIGO_CATEGORIA")
    private Category category;
    @Basic
    @Column(name = "PRECIO")
    private Double price;
    @Basic
    @Column(name = "URL_IMAGEN")
    private String urlImage;
    @Id
    @Column(name = "SKU")
    private String sku;

    public Product() {
    }

    public Product(String name, Category category, Double price, String urlImage, String sku) {
        this.name = name;
        this.category = category;
        this.price = price;
        this.urlImage = urlImage;
        this.sku = sku;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        sku = sku;
    }
}
